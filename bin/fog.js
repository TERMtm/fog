Object.defineProperty(global,"log",{get:function(){return "Yeh, I can keep a secret.";},set:function(a){/*var b=new Date();console.log("At "+b.getSeconds()+"."+b.getMilliseconds()+" Seconds:");*/console.log(a)},configurable:false});
var tl=exports.tl={
    iS:function(a){return typeof a=="string"},
    iF:function(a){return typeof a=="function"},
    iO:function(a){return typeof a=="object"},
    iN:function(a){return typeof a=="number"},
    mD:function(){var a=document.createElement("div"),i=0;for(i=0;i<arguments.length;i++)a.classList.add(arguments[i]);return a},
    mS:function(){return document.createElementNS("http://www.w3.org/2000/svg",arguments[0])},
    mR:function(a){return Math.floor(Math.random()*Math.pow(10,a||3))}},
st=exports.st={c:"class",p:"px",a:"active"},
workingPort=8081,
url=require("url"),
fs=require("fs"),
path=require("path"),
modList=require("../apps/directory.list");

require('http').createServer(function(q,s){
	respond(arguments,modList.root);
}).listen(workingPort);
log="server:ON   port:"+workingPort+"\n";

/*require('https').createServer(function(q,s){
	console.log("wut");
	s.writeHead(200,{"Content-Type":"text/plain"});
	s.write("Hello");
	s.end();
}).listen(413);

		if(typeof a=="object"&&(a=a._resource)){for(i in a)if(this.agent=(t=c[0].headers["user-agent"].indexOf(i)+1)&&i)return;
			this.agent=a._default}else this.agent=a;
*/


function respond(connection,list){
	var pending=4,res=[,,connection,connectionClose,send404];
	function r(input,type){
		res[type]=input;
		if(!--pending)(function(){
			var a=res[5],b=res.shift(),d=res[0],ua=connection[0].headers["user-agent"];
			if(!d.agent&&typeof a=="object"&&(a=a._resource)){if(ua)for(i in a)if((d.agent=(ua.indexOf(i)+1)&&i))break}d.agent=d.agent||"resource";

			b.apply(this,res);
		})();
	}
	route(connection,list,r);
	getSession(connection,list,r);
}
 //hi
function getSession(c,block,callback){
	var a=c[0].headers.cookie,b=null;
	a=a&&a.split(/;\s*/)||[];
	for(var i=0;i<a.length;i++){if((b=a[i].split("="))[0]=="SID"){callback(sessions[b[1]]||new Session(block,c,10800000),1);return}}
	callback(new Session(block,c,10800000),1);
	function Session(a,c,t){
		var session,time=new Date();
		while((session=Math.floor(Math.random()*10000)) in sessions);time.setTime(time.getTime()+t)
		c[1].setHeader("Set-Cookie",["SID="+session+"; expires="+time.toUTCString()+"; httpOnly"]);
		setTimeout(function(){delete sessions[session]},t);
		sessions[session]=this;
	}
}

function getCookie(){
	
}

sessions={};

function route(connection,list,_return){
	log=connection[0].headers.referer;
	log=connection[0].url;
	var dirs,target=(dirs=connection[0].url.replace(/^\/|\/$/g,"").toLowerCase().split("/")).pop()||"_home",a,b,c,req=connection[0];
	log=dirs;
	log=target;
	//log=req.headers;
	var referer=(req.headers.referer||"").toLowerCase().split(/:\/\/|\/+/);
	if(referer[1]==req.headers.host.toLowerCase()){
		dirs=referer.slice(2).concat(dirs);
	}
	while((a=list||a)&&(list=list[dirs.shift()])){
		if(typeof list._override=="function"){
			_return(list._override,0);
			_return(dirs,5);
			_return(target,6);
			return;
		}
	}
	_return(dirs[0]?0:
		(typeof (b=(a[target]||0)._root)=="function"? ((a=a[target])&&false)||b : false)

		||(b=target.split(/\.(?=[^\.]+$)/).length===2 ? 

			(a._noserve&&((a._noserve.name||[]).indexOf(b[1])+1||(a._noserve.type||[]).indexOf(b[0])+1) ? send403 : uploadStatic)	

			:false)
		||send404,
	0);
	_return("apps/"+a._name,5);
	_return(target,6);
}


function connectionClose(a){
	a[1].end();
}

function send404(session,c,callback){
	try{dir._404(c,callback)}
	catch(err){
		log="404"
		c[1].writeHead(404,{"Content-Type":"text-plain"});
		c[1].write("<html><body><h1>404 Yo.</h1></body></html>");
		if(typeof callback==="function")callback(c);
	}
}

function send403(session,c,callback){
	cl("403");
	try{dir._403(c,callback)}
	catch(err){
		cl("OMG FORBIDDEN!")
			
	}	
}

function uploadStatic(session,connection,c200,c404,r,tar,append){
	var d,res=connection[1],d=(typeof session=="string"?[session]:[process.cwd(),r].concat(!session?[]:session.agent)).concat(tar).join("/");
	log=d;
	fs.stat(d,function(err,stat){
		if(stat && stat.isFile()){
			fs.readFile(d,function(a,data){
				var n={"Content-Type":modList.conType[d.match(/\.\w+(?=$)/)[0]]||"text-plain"};
				res.writeHead(200,n);
				res.write(data);
				append&&res.write(append);
				c200(connection);
			})
		}
		else (c404||function(res){
			res[1].writeHead(404);
			res[1].end()})(session,connection,c200);
	})
}

exports.log=log;
exports.uploadStatic=uploadStatic;
exports.route=route;

process.nextTick(function(){
	//var a=require('http').get({hostname:"localhost",path:"/tIDE"/*,'user-agent':"Mozilla/5.0(iPad; U; CPU iPhone OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B314 Safari/531.21.10"*/},function(){});
});

