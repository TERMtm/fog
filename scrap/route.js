try{var url=require("url"),fs=require('fs'),path=require('path'),conType=require('./directory.list').conType}catch(e){throw new Error("Can't get native node or fog module!");}

require('http').createServer(function(q,s){
	console.log(JSON.stringify(q.headers));
	route(arguments,conType.main);
}).listen(cp=80);

//whatever

//create Software Spot

console.log("server:ON   port:"+cp+"\n");

function cl(){for(i=0;i<arguments.length;i++)process.stdout.write((typeof arguments[i]).search("o")?arguments[i]:JSON.stringify(arguments[i])+'\n');return arguments[0]};

function route(connection,list){
	//list=list[url.parse(connection[0].url).hostName.match(/^*+(?=\.\w+\.\w+)/)||"404"];
	var dirs,target=(dirs=connection[0].url.replace(/^\/|\/$/g,"").split("/")).pop()||"_home",a,b,c;
	while((a=list||a)&&(list=list[dirs.shift()])){
		if(!(typeof list._override).search("f")){
			try{list._override(connection,dir.concat(target))}
			catch(e){if(e=="done")return}
		}
	}cl(target,JSON.stringify(a));
	(dirs[0]?0:((typeof (b=(a[target]||0)._root)).search("f")?0:((a=a[target])&&0)||b)||
	(b=target.split(".").length==2&&!cl(typeof a._resource).search("s")?
	((a._noserve.name||[]).indexOf(b[1])+1||(a._noserve.type||[]).indexOf(b[0])+1?send403:uploadStatic):0)||send404)
	(connection,connectionClose,send404,a,target)
}

function connectionClose(){
	arguments[0][1].end();
}

function send404(c,callback,_null,dir,t){
	try{dir._404(c,callback)}
	catch(err){
		c[1].writeHead(404,{"Content-Type":"text-plain"});
		c[1].write("<html><body><h1>404 Yo.</h1></body></html>");
		if(!(typeof callback).search("f"))callback(c);
	}
}

function send403(c,callback,_null,dir,t){
	cl("403");
	try{dir._403(c,callback)}
	catch(err){
		cl("OMG FORBIDDEN!")
			//send 403 page
	}	
}

function uploadStatic(connection,c200,c404,a,tar){
	var d=path.join(process.cwd(),a._resource+"/"+tar),res=connection[1];
	cl(d);
	fs.stat(d,function(err,stat){
		if(stat && stat.isFile()){
			fs.readFile(d,function(a,data){
				var n={"Content-Type":conType[d.match(/\.\w+(?=$)/)[0]]||"text-plain"};
				res.writeHead(200,n);
				res.write(data);
				c200(connection);
			})
		}
		else (c404||function(res){
			res[1].writeHead(404);
			res[1].end()})(connection,c200,null,a,tar);
	})
}

exports.route=route;
exports.uploadStatic=uploadStatic;
//~ exports.global404=global404;
