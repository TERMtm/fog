/*try{*/var cp,url=require("url"),fs=require('fs'),path=require('path'),modList=require('./directory.comments.list')//}catch(e){throw new Error("Can't get native node or fog module!");}

require('http').createServer(function(q,s){
	route(arguments,modList.main);//step one: route the request
	//passes require('./directory.list').main into route, don't know how to actually detect subdomains yet so just direct for now
}).listen(cp=80);
cl("server:ON   port:"+cp+"\n");

function cl(){for(i=0;i<arguments.length;i++)process.stdout.write(typeof arguments[i]=="object"?JSON.stringify(arguments[i])+'\n':arguments[i]);return arguments[0]};
//ok made this to debug and its SUPER helpful
//It basically can enclose any peice of data, litteral or varible, and ship it to console w/o touching it. (it just returns the value) 
//whats great is that it can also work with more than one argument making it great for checking multiple peices of data at any given breakpoint in one shot
//going to work on an actual debug console (AJAX style) sometime, cant wait till thats done
	
function route(connection,list){
	//list=list[url.parse(connection[0].url).hostName.match(/^*+(?=\.\w+\.\w+)/)||"404"];
	var dirs,target=(dirs=connection[0].url.replace(/^\/|\/$/g,"").split("/")).pop()||"_home",a,b,c;
	//this is just a really dence way of saying, ok make my variables
	//target = the last entry of what dirs becomes when set or _home if not existent. This needs to be seperated so that it can be parsed with the assumption that is the last in the query and executed. (OUCH)
	//dirs is an array that is created by splitting the request.url string after it has had its leading and trailing "\"s cut off (OUCH) 
	//a,b, and c are just being declared as local for now
	while((a=list||a)&&(list=list[dirs.shift()])){//now this loop is going to try to enter deeper levels of the directory object based upon the queries in a respective pattern 
	//the while loop contines if it can set the "current directory" as the next one over specified by url
	//the condition preceeding && is just a way to tell the engine to backup the parent directory as "a" by just asking if it can. I back it up because when the condition finally does fail, list will become undefined (thus falsey breaking the loop) 
		if(typeof list._override==="function"){//(THE FOLLOWING BAD IDEA) checks if the _override token exists in cd object and is a function (I used search here because I don't need to sanitize it, and matching the first letter is more efficient than matching the whole word; atleast I would think so has an "f" at the begining of its typeof. In search, it will return index of first found result; 0 being at begining, 1 proceeding first symbol, etc, -1 if not found. since -1 is truey, it can only be falsy if the match is immediatly after line initializor, or string for that matter, '^')   NOT TRUE, FIXED. //BAD if(!(typeof list._override).search("f")) BAD//
			catch(e){if(e=="done")return}//if the override is passive, or fails, the execption is either not thrown or an error and will be skiped over, continuing the directory loop with the override having, say, logged the request. If the override is an end-all, however, it will throw "done" instead of calling back, completing the request independatly.
		}
	}
	//this is a chain of conditional assignments that does all of the heavy lifting to parse the directory.list and find what it can and can't do with the query. After conditional statement is complete, it will push the reference of appropriate function. I'm going to chop it up for you here.
	(//enclose for function call
		dirs[0]?0://check if dirs[0] exists, if so return 0 to represent falsey in order to skip below code until next ||
		(//if it doesn't, loop succeeded (and an agressive _override did not happen) so we can safely look for the _root
			(typeof (b=(a[target]||0)._root)==="function"?//check if _root, OF what b becomes after attempting to be a/*parent directory*/[target] or 0, is a function
				//the reason for the ||0 is typeError prevention, what this is ultimately checking for is two things at once, however if the first part is failing it will still have to check for _root and undefinded._root will throw error, so simple fallback boolean._root will work
				//now this can easily be misconstude
				0://if _root wasn't found to be a function, the search will NOT be falsey but truey. Thus the condition will just do nothing, set itself as 0 (falsey, purposly failing 'or' test)
				((a=a[target])&&0)//if _root was found and is function, the search will be falsey, as previously explained. It sets "a" as "a[target]" (because target is found to a directory like the loops were) and obviously that will represent set value so I put && 0 to purposfuly fail the -or- condition 'again' and it just gets set to b. Basically a way of saying DO THIS TRANSPARENTLY THEN SET w/o breaking syntax of this chain
					||b// remember "b=(a[target]||0)"? if that bit of code fails, this is 0 --officially a 404 or to be treated as a file
		)||//the statement skips here if there is further entries in loop (bad, or _override was called and screwed something up, worse) -OR- if that whole block of code came up empty
		(//these parenthesis block up code where needed so parser knows proper order of opperations
			//this test is looking for whether of not the query is a forbidden file
			b=target.split(/\.(?=[^\.]+$)/).length===2&&typeof a._resource==="string"?//test if both the target (last entry, not having been a directory) has at least one period in it "file.type"
										//^ the regex here splits the string by the LAST period, so that lol.file.txt.wav will be split into ["lol.file.txt","wav"] which is still system recognizable as filename
										//if your wondering the regex works like this "\." find litteral . symbol (don't treat it as token) "(?:   )" that is immediately followed by "[^\.]" anything that is NOT a litteral . symbol "+" as many times as it may occur, "$" untill you find end of the string 
										//      http://tiny.cc/2i8jfw       I just remembered linux files dont need types! 
			//if it was a file type, now I have to check if it was banned or not
			(a._noserve&&(//check if _noserve exists in current directory definition object to avoid typeErrors
				(a._noserve.name||[]).indexOf(b[1])+1//now this checks if .name exists, and if it does check's if b[1]'s (the array from successful split) is found on TSA's no-fly list
					//the +1 is becuase, if present the indexOf will ofcource be true EXCEPT if it's first entry, and -1 is not found (which is also truey)
				||(a._noserve.type||[]).indexOf(b[0])+1//ditto with file type
					//with the ||, either one can make this block true, thus 403 is in effect
			)?send403:uploadStatic)//if 1, check succeeded in finding terrorist activity and is put into holding for further questioning. If 0, no VAC bans are on record for the user and it is free to go.
			:0//if target wasn't a file type, I'm amazed too! :0
		)		
		||send404//if 1. dir[0] was true, 2. _root was not found -AND- target was not found to be a file; stack will collapse and ultimately be falsey (0) thus || will swap it out for send404()
	)
	(connection,connectionClose,send404,a,target)//standard arguments of the above result (being a function reference)
	//([requestObj,responceObj],callback upon success,callback upon not found,the current directory where target derived function is located,the target object if needed)
	//notice argument are ordered from most to least likely to be used by any given function that would show up here, thus can be excluded
}

function connectionClose(){//default "done" callback
	arguments[0][1].end();
}

function send404(c,callback,_null,dir,t){//404 function that attempts _404 then delegates to global 
	try{dir._404(c,callback)}//attempt to do directory specific _404; catch if failed or not existent with global 404 (Where throw("fuuuuuuuuuu") - directory.list:51, comes in where applicable)
	catch(err){
		c[1].writeHead(404,{"Content-Type":"text-plain"});
		c[1].write("<html><body><h1>404 Yo.</h1></body></html>");//Glorious!
		if(!(typeof callback).search("f"))callback(c);
	}
}

//FIX IT SO RECURSIVE OR USELESS!
function send403(c,callback,_null,dir,t){//BROKEN FML
	cl("403");//BROKEN FML
	try{dir._403(c,callback)}//BROKEN FML
	catch(err){//BROKEN FML
		cl("OMG FORBIDDEN!")//BROKEN FML
			//send 403 page//BROKEN FML
	}	//BROKEN FML
}//BROKEN FML

function uploadStatic(connection,c200,c404,a,tar){//function for uploading files (doesn't work for bigger files D:<)
	var d=path.join(process.cwd(),a._resource+"/"+tar),res=connection[1];
	fs.stat(d,function(err,stat){//your basic, check for file and upload, async stuff
		if(stat && stat.isFile()){
			fs.readFile(d,function(a,data){
				var n={"Content-Type":modList.conType[d.match(/\.\w+(?=$)/)[0]]||"text-plain"};
				res.writeHead(200,n);
				res.write(data);
				c200(connection);
			})
		}
		else (c404||function(res){//do callback 404 or simple404 if unspecified (global404 has to be IN callback for it to be run)
			res[1].writeHead(404);
			res[1].end()})(connection,c200,null,a,tar);
	})
}

exports.route=route;
exports.uploadStatic=uploadStatic;
//~ exports.global404=global404;

